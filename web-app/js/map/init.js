$(function () {
    var r = Raphael('map', 510, 465),
        attributes = {
            fill: '#fff',
            stroke: '#3899E6',
            'stroke-width': 1,
            'stroke-linejoin': 'round'
        },
        arr = new Array();
    for (var country in paths) {
        var obj = r.path(paths[country].path);
        //obj.scale(.75,.75);
        obj.attr(attributes);
        arr[obj.id] = country;

        obj
            .hover(function () {
                //document.location.hash = arr[this.id];
                var point = this.getBBox(0);
                $('#map').next('.point').remove();
                $('#map').after($('<div />').addClass('point'));
                var cityName = paths[arr[this.id]].name
                $('.point')
                    .html($('#' + cityName).text())
                    //.prepend($('<a />').attr('href', '#').addClass('close').text('Close'))
                    //.prepend($('<img />').attr('src', 'flags/'+arr[this.id]+'.png'))
                    .css({
                        left: point.x + $('#map').offset().left - 80,
                        top: point.y + $('#map').offset().top - 40

                    })
                //$('.point').fadeIn();
                this.animate({
                    fill: '#1669AD'
                }, 150);
            }, function () {
                $('#map').next('.point').remove();
                this.animate({
                    fill: attributes.fill
                }, 150);
            })
            .click(function () {
                window.location.href = '/region/show/' + paths[arr[this.id]].id;
            });
        $('.point').find('.close').live('click', function () {
            var t = $(this),
                parent = t.parent('.point');
            parent.fadeOut(function () {
                parent.remove();
            });
            return false;
        });
    }
    r.setViewBox(0, 0, 680, 1100);

});