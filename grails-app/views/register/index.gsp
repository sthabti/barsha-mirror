<html>

<head>
    <meta name='layout' content='barsha'/>
    <title><g:message code='spring.security.ui.register.title'/></title>
</head>

<body>

<g:if test='${emailSent}'>
    <div class="alert alert-info">
        <g:message code='spring.security.ui.register.sent'/>
    </div>
</g:if>
<g:else>

    <g:form class="form-horizontal" action='register' name='registerForm'>
        <legend>Register</legend>
        <fieldset>
            <div class="control-group <g:if test="${fieldError(bean: command, field: "email")}">error</g:if>">
                <label class="control-label" for="email">Email:</label>

                <div class="controls">
                    <g:textField id="email" name="email" class="input-xlarge" bean="${command}"
                                 value="${command.email}"/>
                    <span class="help-inline">${fieldError(bean: command, field: "email")}</span>
                </div>
            </div>

            <div class="control-group <g:if test="${fieldError(bean: command, field: "password")}">error</g:if>">
                <label class="control-label" for="email">Password:</label>

                <div class="controls">
                    <g:passwordField id="password" name="password" class="input-xlarge" bean="${command}"
                                     value="${command.password}"/>
                    <span class="help-inline">${fieldError(bean: command, field: "password")}</span>
                </div>
            </div>

            <div class="control-group <g:if test="${fieldError(bean: command, field: "password2")}">error</g:if>">
                <label class="control-label" for="email">Password:</label>

                <div class="controls">
                    <g:passwordField id="password2" name="password2" class="input-xlarge" bean="${command}"
                                     value="${command.password2}"/>
                    <span class="help-inline">${fieldError(bean: command, field: "password2")}</span>
                </div>
            </div>

            <div class="form-actions">
                <g:submitButton id="create-submit" class="btn btn-primary" alt="Create a new account" name="create"
                                value="Create Account"/>
            </div>
        </fieldset>
    </g:form>
</g:else>
<script>
    $(document).ready(function () {
        $('#username').focus();
    });
</script>

</body>
</html>
