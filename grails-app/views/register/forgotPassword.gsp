<html>

<head>
    <title><g:message code='spring.security.ui.forgotPassword.title'/></title>
    <meta name='layout' content='barsha'/>
</head>

<body>
<div class="page-header">
    <h1 class="pageHeading">Forgot Password</h1>
</div>

<g:if test='${emailSent}'>
    <bootstrap:alert class="alert-info"><g:message code='spring.security.ui.forgotPassword.sent'/></bootstrap:alert>
</g:if>

<g:form action='forgotPassword' name="forgotPasswordForm" autocomplete='on'>
    <h4><g:message code='spring.security.ui.forgotPassword.description'/></h4>
    <label for="username"><g:message code='spring.security.ui.forgotPassword.username'/>
        <sec:ifLoggedIn>
            <g:textField name="username" value="${sec.username()}"></g:textField>
        </sec:ifLoggedIn>
        <sec:ifNotLoggedIn>
            <g:textField name="username"/>
        </sec:ifNotLoggedIn></label>

    <div class="form-actions">
        <g:submitButton elementId='reset' data-toggle="button" name="reset" class="btn btn-primary"
                        form='forgotPasswordForm'
                        value="${message(code: 'spring.security.ui.forgotPassword.submit')}"/>
    </div>
</g:form>
<script>
    $(document).ready(function () {
        $('#username').focus();
    });
</script>
</body>
</html>
