<!doctype html>
<html>
<head>
    <meta name="layout" content="barsha">
    <g:set var="entityName" value="${message(code: 'post.label', default: 'Post')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<div class="row-fluid">

    %{--<div class="span3">--}%
    %{--<div class="well">--}%
    %{--<ul class="nav nav-list">--}%
    %{--<li class="nav-header">${entityName}</li>--}%
    %{--<li>--}%
    %{--<g:link class="list" action="list">--}%
    %{--<i class="icon-list"></i>--}%
    %{--<g:message code="default.list.label" args="[entityName]" />--}%
    %{--</g:link>--}%
    %{--</li>--}%
    %{--<li>--}%
    %{--<g:link class="create" action="create">--}%
    %{--<i class="icon-plus"></i>--}%
    %{--<g:message code="default.create.label" args="[entityName]" />--}%
    %{--</g:link>--}%
    %{--</li>--}%
    %{--</ul>--}%
    %{--</div>--}%
    %{--</div>--}%

    <div>

        <div class="page-header">
            <h1><g:message code="default.show.label" args="[entityName]"/></h1>
        </div>

        <g:if test="${flash.message}">
            <bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
        </g:if>

        <div>
            <h1>${postInstance?.title}</h1>
            <i>${postInstance?.dateCreated}</i></div>

        <h3>Description</h3>

        <div>
            <div style="float: right">

                <img src="${postInstance?.imageUrl}" class="img-polaroid">

            </div>
            ${postInstance?.description}
        </div>
        <br/>
        <h5>Galary</h5>

        <div class=" table-bordered">
            <img data-src="holder.js/260x180" alt="260x180" src="http://i.imgur.com/S983j.jpg">
            <img data-src="holder.js/260x180" alt="260x180" src="http://i.imgur.com/S983j.jpg">
            <img data-src="holder.js/260x180" alt="260x180" src="http://i.imgur.com/S983j.jpg">
            <img data-src="holder.js/260x180" alt="260x180" src="http://i.imgur.com/S983j.jpg">
            <img data-src="holder.js/260x180" alt="260x180" src="http://i.imgur.com/S983j.jpg">
        </div>

        <h5>Details</h5>
        <table class="table table-nonfluid table-bordered">
            <tr><td><strong>Price:</strong><span class="label label-success">5000</span></td></tr>
            <tr><td><strong>Region:</strong><span class="label label-success">${postInstance?.region}</span></td></tr>
            <tr><td><strong>City:</strong><span class="label label-info">${postInstance?.city}</span></td></tr>
            <tr><td><strong>Telephone:</strong><span class="label label-info"><i
                    class="icon-bullhorn"></i>${postInstance?.title}</span></td></tr>
            <tr><td><strong>Email:</strong><span class="label label-info"><i
                    class="icon-envelope"></i>${postInstance.user}</span></td></tr>
        </table>
        <hr/>

        <h6>Social</h6>

        <div class=" table-bordered">
            <img data-src="holder.js/260x180" alt="260x180" src="http://i.imgur.com/M1WCh.gif">
            <img data-src="holder.js/260x180" alt="260x180" src="http://i.imgur.com/cZvsp.gif">
        </div>


        <g:if test="${postUser == postInstance.user}">
            <g:form>
                <g:hiddenField name="id" value="${postInstance?.id}"/>
                <div class="form-actions">
                    <g:link class="btn" action="edit" id="${postInstance?.id}">
                        <i class="icon-pencil"></i>
                        <g:message code="default.button.edit.label" default="Edit"/>
                    </g:link>
                    <button class="btn btn-danger" type="submit" name="_action_delete">
                        <i class="icon-trash icon-white"></i>
                        <g:message code="default.button.delete.label" default="Delete"/>
                    </button>
                </div>
            </g:form>
        </g:if>
        <g:else>

            <sec:ifAllGranted roles="ROLE_ADMIN">
                <g:form>
                    <g:hiddenField name="id" value="${postInstance?.id}"/>
                    <div class="form-actions">
                        <g:link class="btn" action="edit" id="${postInstance?.id}">
                            <i class="icon-pencil"></i>
                            <g:message code="default.button.edit.label" default="Edit"/>
                        </g:link>
                        <button class="btn btn-danger" type="submit" name="_action_delete">
                            <i class="icon-trash icon-white"></i>
                            <g:message code="default.button.delete.label" default="Delete"/>
                        </button>
                    </div>
                </g:form>
            </sec:ifAllGranted>
        </g:else>

    </div>

</div>
</body>
</html>
