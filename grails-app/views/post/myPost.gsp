<html>
<head>
    <meta name="layout" content="barsha">
    <title>Show Posts</title>
</head>

<body>
<div class="row-fluid">
    <g:if test="${flash.message}">
        <bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
    </g:if>
    <div class="page-header">
        <h1>Posts <small>Your Posts</small></h1>
    </div>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Photo</th>
            <th>Title</th>
            <th>Description</th>
            <th>City</th>
            <th>Telephone</th>
            <th>Price</th>
            <th>Category</th>
        </tr>
        </thead>
        <tbody>

        <g:each in="${posts}" var="post">
            <tr>
                <td><a href="${createLink(controller: 'post', action: 'show', id: post.id)}"><img width="150"
                                                                                                  src="${post?.imageUrl?.encodeAsHTML()}"
                                                                                                  class="img-polaroid">
                </a>
                </td>
                <td><a href="${createLink(controller: 'post', action: 'show', id: post.id)}">${post.title}</a></td>
                <td>${post.description}</td>
                <td>${post.city}</td>
                <td>${post.telephone}</td>
                <td>${post.price}</td>
                <td>${post.category}</td>
            </tr>
        %{--<div class="pagination">--}%
        %{--<bootstrap:paginate total="${postTotal}" />--}%
        %{--</div>--}%
        </g:each>

        </tbody>
    </table>
</div>
</body>
</html>
