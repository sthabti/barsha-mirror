<%@ page import="barsha.Post" %>



<div class="fieldcontain ${hasErrors(bean: postInstance, field: 'title', 'error')} required">
    <label for="title">
        <g:message code="post.title.label" default="Title"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="title" required="" value="${postInstance?.title}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: postInstance, field: 'description', 'error')} required">
    <label for="description">
        <g:message code="post.description.label" default="Description"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="description" required="" value="${postInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: postInstance, field: 'city', 'error')} required">
    <label for="city">
        <g:message code="post.city.label" default="City"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="city" required="" value="${postInstance?.city}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: postInstance, field: 'telephone', 'error')} required">
    <label for="telephone">
        <g:message code="post.telephone.label" default="Telephone"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="telephone" required="" value="${postInstance?.telephone}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: postInstance, field: 'price', 'error')} required">
    <label for="price">
        <g:message code="post.price.label" default="Price"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field name="price" value="${fieldValue(bean: postInstance, field: 'price')}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: postInstance, field: 'category', 'error')} required">
    <label for="category">
        <g:message code="post.category.label" default="Category"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="category" name="category.id" from="${barsha.Category.list()}" optionKey="id" required=""
              value="${postInstance?.category?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: postInstance, field: 'created', 'error')} required">
    <label for="dateCreated">
        <g:message code="post.created.label" default="dateCreated"/>
        <span class="required-indicator">*</span>
    </label>
    <g:datePicker name="dateCreated" precision="day" value="${postInstance?.dateCreated}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: postInstance, field: 'imageUrl', 'error')} ">
    <label for="imageUrl">
        <g:message code="post.imageUrl.label" default="Image Url"/>

    </label>
    <g:textField name="imageUrl" value="${postInstance?.imageUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: postInstance, field: 'region', 'error')} required">
    <label for="region">
        <g:message code="post.region.label" default="Region"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="region" name="region.id" from="${barsha.Region.list()}" optionKey="id" required=""
              value="${postInstance?.region?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: postInstance, field: 'user', 'error')} required">
    <label for="user">
        <g:message code="post.user.label" default="User"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="user" name="user.id" from="${barsha.User.list()}" optionKey="id" required=""
              value="${postInstance?.user?.id}" class="many-to-one"/>
</div>

