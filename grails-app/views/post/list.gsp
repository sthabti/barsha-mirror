<%@ page import="barsha.Post" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="barsha">
    <g:set var="entityName" value="${message(code: 'post.label', default: 'Post')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="row-fluid">
    %{----}%
    %{--<div class="span3">--}%
    %{--<div class="well">--}%
    %{--<ul class="nav nav-list">--}%
    %{--<li class="nav-header">${entityName}</li>--}%
    %{--<li class="active">--}%
    %{--<g:link class="list" action="list">--}%
    %{--<i class="icon-list icon-white"></i>--}%
    %{--<g:message code="default.list.label" args="[entityName]" />--}%
    %{--</g:link>--}%
    %{--</li>--}%
    %{--<li>--}%
    %{--<g:link class="create" action="create">--}%
    %{--<i class="icon-plus"></i>--}%
    %{--<g:message code="default.create.label" args="[entityName]" />--}%
    %{--</g:link>--}%
    %{--</li>--}%
    %{--</ul>--}%
    %{--</div>--}%
    %{--</div>--}%

    <div>

        <div class="page-header">
            <h1><g:message code="default.list.label" args="[entityName]"/></h1>
        </div>

        <g:if test="${flash.message}">
            <bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
        </g:if>

        <table class="table table-hover">
            <thead>
            <tr>

                <g:sortableColumn property="Photo" title="${message(code: 'post.photo.label', default: 'Photo')}"/>

                <g:sortableColumn property="title" title="${message(code: 'post.title.label', default: 'Title')}"/>

                <g:sortableColumn property="description"
                                  title="${message(code: 'post.description.label', default: 'Description')}"/>

                <g:sortableColumn property="city" title="${message(code: 'post.city.label', default: 'City')}"/>

                <g:sortableColumn property="telephone"
                                  title="${message(code: 'post.telephone.label', default: 'Telephone')}"/>

                <g:sortableColumn property="price" title="${message(code: 'post.price.label', default: 'Price')}"/>

                <th class="header"><g:message code="post.category.label" default="Category"/></th>

            </tr>
            </thead>
            <tbody>
            <g:each in="${postInstanceList}" var="postInstance">
                <tr>
                    <td><a href="${createLink(controller: 'post', action: 'show', id: postInstance.id)}"><img
                            width="150" src="${fieldValue(bean: postInstance, field: "imageUrl")}" class="img-polaroid">
                    </a></td>

                    <td><a href="${createLink(controller: 'post', action: 'show', id: postInstance.id)}">${fieldValue(bean: postInstance, field: "title")}</a>
                    </td>

                    <td>${fieldValue(bean: postInstance, field: "description")}</td>

                    <td>${fieldValue(bean: postInstance, field: "city")}</td>

                    <td>${fieldValue(bean: postInstance, field: "telephone")}</td>

                    <td>${fieldValue(bean: postInstance, field: "price")}</td>

                    <td>${fieldValue(bean: postInstance, field: "category")}</td>

                </tr>
            </g:each>
            </tbody>
        </table>

        <div class="pagination">
            <bootstrap:paginate total="${postInstanceTotal}"/>
        </div>
    </div>

</div>
</body>
</html>
