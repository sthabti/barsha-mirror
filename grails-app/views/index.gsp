<%@ page import="barsha.Region" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="barsha"/>
    <title>Welcome to Barsha</title>
    <script src="${resource(dir: 'js/map', file: 'raphael-min.js')}" type="text/javascript"></script>
    <script src="${resource(dir: 'js/map', file: 'tunisia.js')}" type="text/javascript"></script>
    <script src="${resource(dir: 'js/map', file: 'init.js')}" type="text/javascript"></script>
</head>

<body>
<div class="pull-right">
    <img src="http://i.imgur.com/OPM36.jpg"/>

</div>

<div id="map"></div>
<ul class="pull-left ">
    <strong>Tunisian Region's :</strong>
    <g:each var="region" in="${Region.list()}">
        <a href="${createLink(controller: 'region', action: 'show', id: region.uri)}">${region.name}</a> -
    </g:each>
</body>
</html>
