<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><g:layoutTitle default="Barsha"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
    <link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
    <link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
    <link rel="stylesheet" href="${resource(dir: 'bootstrap/css', file: 'bootstrap.min.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'barsha.css')}" type="text/css">
    <script src="${resource(dir: 'js', file: 'jquery-1.8.3.min.js')}" type="text/javascript"></script>
    <script src="${resource(dir: 'bootstrap/js', file: 'bootstrap.min.js')}" type="text/javascript"></script>
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-37522401-1']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();

    </script>
    <g:layoutHead/>
    <r:layoutResources/>
</head>

<body>
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">

            <a class="brand" href="/">Barsha</a>

            <div class="nav-collapse collapse">
                <ul class="nav">
                    <li><a href="/">Home</a></li>
                    <li><a href="${createLink(controller: 'post', action: 'create')}">Create</a></li>

                </ul>
                <ul class="nav pull-right">
                    <sec:ifNotLoggedIn>
                        <li><a href="${createLink(controller: 'login', action: 'auth')}">Login</a></li>
                        <li><a href="${createLink(controller: 'register', action: 'index')}">Register</a></li>

                    </sec:ifNotLoggedIn>
                    <sec:ifLoggedIn>
                        <li><a href="/user/posts">Your Posts</a></li>
                        <li class="dropdown">
                            <a href="#" id="drop2" role="button" class="dropdown-toggle"
                               data-toggle="dropdown">Account<b class="caret"></b></a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="drop2">
                                <li><a tabindex="-1"
                                       href="${createLink(controller: 'register', action: 'forgotPassword')}">Forgot Password</a>
                                </li>
                                <li class="divider"></li>
                                <li><a href="${createLink(controller: 'logout')}">Logout</a></li>
                            </ul>
                        </li>

                    </sec:ifLoggedIn>
                </ul>

            </div>

        </div>
    </div>
</div>

<div class="container main">
    <g:layoutBody/>
</div>

<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
<g:javascript library="application"/>
<r:layoutResources/>
</body>
</html>