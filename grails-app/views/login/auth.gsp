<html>
<head>
    <meta name='layout' content='barsha'/>
    <title><g:message code="springSecurity.login.title"/></title>
</head>

<body>
<div>
    <div>
        <g:if test="${flash.message}">
            <bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
        </g:if>

        <form class="form-horizontal" action='${postUrl}' method='POST' id='loginForm' autocomplete='on'>
            <legend><g:message code="springSecurity.login.header"/></legend>

            <div class="control-group">
                <label class="control-label" for='username'><g:message
                        code="springSecurity.login.username.label"/>:</label>

                <div class="controls">
                    <input type='text' class='text_' name='j_username' id='username' placeholder="Username">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for='password'><g:message
                        code="springSecurity.login.password.label"/>:</label>

                <div class="controls">
                    <input type='password' class='text_' name='j_password' id='password' placeholder="Password"/>
                </div>
            </div>

            <div class="control-group">
                <div class="controls">
                    <label class="checkbox">
                        <input type='checkbox' name='${rememberMeParameter}' id='remember_me'
                               <g:if test='${hasCookie}'>checked='checked'</g:if>/> <g:message
                            code="springSecurity.login.remember.me.label"/></label>
                </label>

                </div>

                <div class="form-actions">
                    <input class="btn btn-primary" type='submit' id="submit"
                           value='${message(code: "springSecurity.login.button")}'/>
                    <a id="register" class="btn" href="${createLink(controller: 'register')}">Register</a>
                    <a id="forgot-password" class="btn" rel="Visit the forgot password page"
                       href="${createLink(controller: 'register', action: 'forgotPassword')}">Forgot Password</a>
                </div>
            </div>
        </form>
    </div>
</div>
<script type='text/javascript'>
    <!--
    (function () {
        document.forms['loginForm'].elements['j_username'].focus();
    })();
    // -->
</script>
</body>
</html>
