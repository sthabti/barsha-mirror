<%@ page import="barsha.Region" %>



<div class="fieldcontain ${hasErrors(bean: regionInstance, field: 'name', 'error')} required">
    <label for="name">
        <g:message code="region.name.label" default="Name"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="name" required="" value="${regionInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: regionInstance, field: 'post', 'error')} ">
    <label for="post">
        <g:message code="region.post.label" default="Post"/>

    </label>

    <ul class="one-to-many">
        <g:each in="${regionInstance?.post ?}" var="p">
            <li><g:link controller="post" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
        </g:each>
        <li class="add">
            <g:link controller="post" action="create"
                    params="['region.id': regionInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'post.label', default: 'Post')])}</g:link>
        </li>
    </ul>

</div>

