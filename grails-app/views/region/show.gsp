<%@ page import="barsha.Region" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="barsha">
    <g:set var="entityName" value="${message(code: 'region.label', default: 'Region')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<div class="row-fluid">

    %{--<div class="span3">--}%
    %{--<div class="well">--}%
    %{--<ul class="nav nav-list">--}%
    %{--<li class="nav-header">${entityName}</li>--}%
    %{--<li>--}%
    %{--<g:link class="list" action="list">--}%
    %{--<i class="icon-list"></i>--}%
    %{--<g:message code="default.list.label" args="[entityName]" />--}%
    %{--</g:link>--}%
    %{--</li>--}%
    %{--<li>--}%
    %{--<g:link class="create" action="create">--}%
    %{--<i class="icon-plus"></i>--}%
    %{--<g:message code="default.create.label" args="[entityName]" />--}%
    %{--</g:link>--}%
    %{--</li>--}%
    %{--</ul>--}%
    %{--</div>--}%
    %{--</div>--}%

    <div>

        <div class="page-header">
            <h1>Region: <small><g:fieldValue bean="${regionInstance}" field="name"/></small></h1>
        </div>

        <g:if test="${flash.message}">
            <bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
        </g:if>
        <table class="table table-hover">
            <thead>
            <tr>

                <g:sortableColumn property="Photo" title="${message(code: 'post.photo.label', default: 'Photo')}"/>

                <g:sortableColumn property="title" title="${message(code: 'post.title.label', default: 'Title')}"/>

                <g:sortableColumn property="description"
                                  title="${message(code: 'post.description.label', default: 'Description')}"/>

                <g:sortableColumn property="city" title="${message(code: 'post.city.label', default: 'City')}"/>

                <g:sortableColumn property="telephone"
                                  title="${message(code: 'post.telephone.label', default: 'Telephone')}"/>

                <g:sortableColumn property="price" title="${message(code: 'post.price.label', default: 'Price')}"/>

                <g:sortableColumn property="category"
                                  title="${message(code: 'post.category.label', default: 'Category')}"/>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${regionInstance.post}" var="p">

                <tr>
                    <td><a href="${createLink(controller: 'post', action: 'show', id: p.id)}"><img width="150"
                                                                                                   src="${p?.imageUrl?.encodeAsHTML()}"
                                                                                                   class="img-polaroid">
                    </a></td>
                    <td><a href="${createLink(controller: 'post', action: 'show', id: p.id)}">${p?.title?.encodeAsHTML()}</a>
                    </td>
                    <td>${p?.description?.encodeAsHTML()}</td>

                    <td>${p?.city?.encodeAsHTML()}</td>

                    <td>${p?.telephone?.encodeAsHTML()}</td>

                    <td>${p?.price?.encodeAsHTML()}</td>

                    <td>${p?.category?.encodeAsHTML()}</td>

                </tr>

            </g:each>
            </tbody>
        </table>
        <sec:ifAllGranted roles="ROLE_ADMIN">
            <g:form>
                <g:hiddenField name="id" value="${regionInstance?.id}"/>
                <div class="form-actions">
                    <g:link class="btn" action="edit" id="${regionInstance?.id}">
                        <i class="icon-pencil"></i>
                        <g:message code="default.button.edit.label" default="Edit"/>
                    </g:link>
                    <button class="btn btn-danger" type="submit" name="_action_delete">
                        <i class="icon-trash icon-white"></i>
                        <g:message code="default.button.delete.label" default="Delete"/>
                    </button>
                </div>
            </g:form>
        </sec:ifAllGranted>
    </div>

</div>
</body>
</html>
