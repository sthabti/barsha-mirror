package barsha

import grails.plugins.springsecurity.Secured
import org.springframework.dao.DataIntegrityViolationException

class RegionController {

    static allowedMethods = [create: ['GET', 'POST'], edit: ['GET', 'POST'], delete: 'POST']

    def index() {
        redirect action: 'list', params: params
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [regionInstanceList: Region.list(params), regionInstanceTotal: Region.count()]
    }

    @Secured(['IS_AUTHENTICATED_REMEMBERED'])
    def create() {
        switch (request.method) {
            case 'GET':
                [regionInstance: new Region(params)]
                break
            case 'POST':
                def regionInstance = new Region(params)
                if (!regionInstance.save(flush: true)) {
                    render view: 'create', model: [regionInstance: regionInstance]
                    return
                }

                flash.message = message(code: 'default.created.message', args: [message(code: 'region.label', default: 'Region'), regionInstance.id])
                redirect action: 'show', id: regionInstance.id
                break
        }
    }

    def show() {
        def regionInstance = Region.findByUri(params.id)
        if (!regionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'region.label', default: 'Region'), params.id])
            redirect action: 'list'
            return
        }

        [regionInstance: regionInstance]
    }

    @Secured(['ROLE_ADMIN'])
    def edit() {
        switch (request.method) {
            case 'GET':
                def regionInstance = Region.get(params.id)
                if (!regionInstance) {
                    flash.message = message(code: 'default.not.found.message', args: [message(code: 'region.label', default: 'Region'), params.id])
                    redirect action: 'list'
                    return
                }

                [regionInstance: regionInstance]
                break
            case 'POST':
                def regionInstance = Region.get(params.id)
                if (!regionInstance) {
                    flash.message = message(code: 'default.not.found.message', args: [message(code: 'region.label', default: 'Region'), params.id])
                    redirect action: 'list'
                    return
                }

                if (params.version) {
                    def version = params.version.toLong()
                    if (regionInstance.version > version) {
                        regionInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
                                [message(code: 'region.label', default: 'Region')] as Object[],
                                "Another user has updated this Region while you were editing")
                        render view: 'edit', model: [regionInstance: regionInstance]
                        return
                    }
                }

                regionInstance.properties = params

                if (!regionInstance.save(flush: true)) {
                    render view: 'edit', model: [regionInstance: regionInstance]
                    return
                }

                flash.message = message(code: 'default.updated.message', args: [message(code: 'region.label', default: 'Region'), regionInstance.id])
                redirect action: 'show', id: regionInstance.id
                break
        }
    }

    @Secured(['ROLE_ADMIN'])
    def delete() {
        def regionInstance = Region.get(params.id)
        if (!regionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'region.label', default: 'Region'), params.id])
            redirect action: 'list'
            return
        }

        try {
            regionInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'region.label', default: 'Region'), params.id])
            redirect action: 'list'
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'region.label', default: 'Region'), params.id])
            redirect action: 'show', id: params.id
        }
    }
}
