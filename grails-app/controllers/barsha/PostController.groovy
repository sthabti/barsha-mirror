package barsha

import grails.plugins.springsecurity.Secured
import org.springframework.dao.DataIntegrityViolationException

class PostController {

    def springSecurityService

    static allowedMethods = [create: ['GET', 'POST'], edit: ['GET', 'POST'], delete: 'POST']

    def index() {
        redirect action: 'list', params: params
    }

    @Secured(['IS_AUTHENTICATED_REMEMBERED'])
    def myPost() {
        User user = springSecurityService.currentUser
        List<Post> userPosts = Post.findAllByUser(user)

        [posts: userPosts]
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [postInstanceList: Post.list(params), postInstanceTotal: Post.count()]
    }

    @Secured(['IS_AUTHENTICATED_REMEMBERED'])
    def create() {
        switch (request.method) {
            case 'GET':
                [postInstance: new Post(params)]
                break
            case 'POST':
                def postInstance = new Post(params)
                postInstance.user = springSecurityService.currentUser
                if (!postInstance.save(flush: true)) {
                    render view: 'create', model: [postInstance: postInstance]
                    return
                }

                flash.message = message(code: 'default.created.message', args: [message(code: 'post.label', default: 'Post'), postInstance.id])
                redirect action: 'show', id: postInstance.id
                break
        }
    }

    def show() {
        def postInstance = Post.get(params.id)
        if (!postInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'post.label', default: 'Post'), params.id])
            redirect action: 'list'
            return
        }
        def currentUser = springSecurityService.currentUser

        [postInstance: postInstance, postUser: currentUser]
    }

    @Secured(['IS_AUTHENTICATED_REMEMBERED'])
    def edit() {
        switch (request.method) {
            case 'GET':
                def postInstance = Post.get(params.id)
                if (!postInstance) {
                    flash.message = message(code: 'default.not.found.message', args: [message(code: 'post.label', default: 'Post'), params.id])
                    redirect action: 'list'
                    return
                } else {
                    def user = springSecurityService.currentUser
                    if (user != postInstance.user) {
                        render(view: "/login/denied")
                    }
                }
                [postInstance: postInstance]
                break
            case 'POST':
                def postInstance = Post.get(params.id)
                if (!postInstance) {
                    flash.message = message(code: 'default.not.found.message', args: [message(code: 'post.label', default: 'Post'), params.id])
                    redirect action: 'list'
                    return
                }

                if (params.version) {
                    def version = params.version.toLong()
                    if (postInstance.version > version) {
                        postInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
                                [message(code: 'post.label', default: 'Post')] as Object[],
                                "Another user has updated this Post while you were editing")
                        render view: 'edit', model: [postInstance: postInstance]
                        return
                    }
                }

                postInstance.properties = params

                if (!postInstance.save(flush: true)) {
                    render view: 'edit', model: [postInstance: postInstance]
                    return
                }

                flash.message = message(code: 'default.updated.message', args: [message(code: 'post.label', default: 'Post'), postInstance.id])
                redirect action: 'show', id: postInstance.id
                break
        }
    }

    def delete() {
        def postInstance = Post.get(params.id)
        if (!postInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'post.label', default: 'Post'), params.id])
            redirect action: 'list'
            return
        }

        try {
            postInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'post.label', default: 'Post'), params.id])
            redirect action: 'list'
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'post.label', default: 'Post'), params.id])
            redirect action: 'show', id: params.id
        }
    }
}
