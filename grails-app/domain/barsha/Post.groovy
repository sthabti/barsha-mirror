package barsha

class Post {

    String title
    String description
    String city
    String telephone
    BigDecimal price
    Date dateCreated
    String imageUrl

    Category category
    Region region

    static belongsTo = [user: User]

    static constraints = {
        title blank: false, nullable: false
        description blank: false, nullable: false
        city blank: false, nullable: false
        telephone blank: false, nullable: false
        price blank: false, nullable: false
        user display: false, nullable: true, blank: false
        dateCreated display: false, nullable: true, blank: false
    }
}
