package barsha

class Region {
    String name
    String uri

    static hasMany = [post: Post]


    static constraints = {
        name blank: false, nullable: false, unique: true
    }

    String toString() {
        return name
    }


}
