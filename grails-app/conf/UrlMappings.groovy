class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?" {
            constraints {
                // apply constraints here
            }
        }

        "/"(view: "/index")
        "/user/posts"(controller: 'post', action: 'myPost')
        "500"(view: '/error')
    }
}
