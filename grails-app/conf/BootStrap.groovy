import barsha.User
import barsha.Role
import barsha.Post
import barsha.UserRole
import barsha.Region
import barsha.Category

class BootStrap {

    def init = { servletContext ->
        def adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true)
        def userRole = new Role(authority: 'ROLE_USER').save(flush: true)
        def monitorRole = new Role(authority: 'ROLE_MONITOR').save(flush: true)


        def admin = new User(email: "admin@barsha.tn", username: 'admin@barsha.tn', enabled: true, password: 'barsha').save(flush: true)
        def user = new User(email: "user@barsha.tn", username: 'user@barsha.tn', enabled: true, password: 'barsha').save(flush: true)
        def monitor = new User(email: "monitor@gmail.com", username: 'monitor@gmail.com', enabled: true, password: 'barsha').save(flush: true)

        UserRole.create admin, adminRole, true
        UserRole.create user, userRole, true
        UserRole.create monitor, monitorRole, true
//region
        def ariana = new Region(name: "Ariana", uri: "ariana").save(flush: true)
        def bejas = new Region(name: "Bejas", uri: "bejas").save(flush: true)
        def benArous = new Region(name: "Ben Arous", uri: "ben-arous").save(flush: true)
        def bizerte = new Region(name: "Bizerte", uri: "bizerte").save(flush: true)
        def gabes = new Region(name: "Gabes", uri: "gabes").save(flush: true)
        def gafsa = new Region(name: "Gafsa", uri: "gafsa").save(flush: true)
        def jendouba = new Region(name: "Jendouba", uri: "jendouba").save(flush: true)
        def kairouan = new Region(name: "kairouan", uri: "kairouan").save(flush: true)
        def kasserine = new Region(name: "Kasserine", uri: "kasserine").save(flush: true)
        def kebili = new Region(name: "Kebili", uri: "kebili").save(flush: true)
        def kef = new Region(name: "Kef", uri: "kef").save(flush: true)
        def mahdia = new Region(name: "Mahdia", uri: "mahdia").save(flush: true)
        def manouba = new Region(name: "Manouba", uri: "manouba").save(flush: true)
        def medenine = new Region(name: "Medenine", uri: "medenine").save(flush: true)
        def monastir = new Region(name: "Monastir", uri: "monastir").save(flush: true)
        def nabeul = new Region(name: "Nabeul", uri: "nabeul").save(flush: true)
        def sfax = new Region(name: "Sfax", uri: "sfax").save(flush: true)
        def sidiBouZid = new Region(name: "Sidi Bou Zid", uri: "sidi-bou-zid").save(flush: true)
        def siliana = new Region(name: "Siliana", uri: "siliana").save(flush: true)
        def sousse = new Region(name: "Sousse", uri: "sousse").save(flush: true)
        def tataouine = new Region(name: "Tataouine", uri: "tataouine").save(flush: true)
        def tozeur = new Region(name: "Tozeur", uri: "tozeur").save(flush: true)
        def tunis = new Region(name: "Tunis", uri: "tunis").save(flush: true)
        def zaghouan = new Region(name: "Zaghouan", uri: "zaghouan").save(flush: true)

//category
        def jobs = new Category(name: "Jobs").save(flush: true)
        def technology = new Category(name: "Technology").save(flush: true)
        def service = new Category(name: "Service").save(flush: true)
//post

        def postA = new Post(title: "cat", description: "Amazing cat", city: "DOha", telephone: "243.00", created: new Date(), price: 232.4, imageUrl: "http://i.imgur.com/0xyde.jpg", category: technology, region: bizerte, user: admin).save(flush: true)
        def postAB = new Post(title: "cat", description: "Amazing cat", city: "DOha", telephone: "243.00", created: new Date(), price: 232.4, imageUrl: "http://i.imgur.com/0xyde.jpg", category: technology, region: bizerte, user: user).save(flush: true)
        def postABC = new Post(title: "cat", description: "Amazing cat", city: "DOha", telephone: "243.00", created: new Date(), price: 232.4, imageUrl: "http://i.imgur.com/0xyde.jpg", category: technology, region: bizerte, user: admin).save(flush: true)
        def postABCD = new Post(title: "cat", description: "Amazing cat", city: "DOha", telephone: "243.00", created: new Date(), price: 232.4, imageUrl: "http://i.imgur.com/0xyde.jpg", category: technology, region: bizerte, user: user).save(flush: true)
        def postABCDE = new Post(title: "cat", description: "Amazing cat", city: "DOha", telephone: "243.00", created: new Date(), price: 232.4, imageUrl: "http://i.imgur.com/0xyde.jpg", category: technology, region: bizerte, user: admin).save(flush: true)
        def postABCDEF = new Post(title: "cat", description: "Amazing cat", city: "DOha", telephone: "243.00", created: new Date(), price: 232.4, imageUrl: "http://i.imgur.com/0xyde.jpg", category: technology, region: bizerte, user: admin).save(flush: true)
        def postTwo = new Post(title: "Deog", description: "Amazing dog", city: "DOha", telephone: "24343.00", created: new Date(), price: 232.4, imageUrl: "http://i.imgur.com/0xyde.jpg", category: jobs, region: bizerte, user: user).save(flush: true)
        def postThree = new Post(title: "Car", description: "Cheap Car", city: "DOha", telephone: "2453.00", created: new Date(), price: 232.4, imageUrl: "http://i.imgur.com/I0XHV.png", category: service, region: bizerte, user: monitor).save(flush: true)
    }
    def destroy = {
    }
}
